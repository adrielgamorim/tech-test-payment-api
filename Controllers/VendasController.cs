using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendasController : ControllerBase
    {
        private readonly VendasContext _context;
        public VendasController(VendasContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var result = _context.Vendas.Include(v => v.Vendedor).Where(x => x.Id == id);
            if(result == null)return NotFound();
            return Ok(result);
        }

        [HttpGet("ObterTodos")]
        public IActionResult ObterTodos()
        {
            var result = _context.Vendas.Include(v => v.Vendedor);
            if(result.Count() < 1) return NotFound();
            return Ok(result);
        }

        [HttpGet("ObterPorProduto")]
        public IActionResult ObterPorProduto(string produto)
        {
            var result = _context.Vendas.Include(v => v.Vendedor).Where(x => x.Produtos.Contains(produto));
            return Ok(result);
        }

        [HttpGet("ObterPorStatus")]
        public IActionResult ObterPorStatus(EnumStatusVenda status)
        {
            var result = _context.Vendas.Include(v => v.Vendedor).Where(x => x.Status == status);
            return Ok(result);
        }

        [HttpPost("RegistrarVenda")]
        public IActionResult Criar(Venda venda)
        {
            if (venda.DataVenda == DateTime.MinValue)
                return BadRequest(new { Erro = "A data da venda não pode ser vazia" });

            _context.Add(venda);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterPorId), new { id = venda.Id }, venda);
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Venda venda)
        {
            var result = _context.Vendas.Find(id);
            if (result == null) return NotFound();

            if (venda.DataVenda == DateTime.MinValue)
                return BadRequest(new { Erro = "A data da venda não pode ser vazia" });

            result.Vendedor = venda.Vendedor;
            result.DataVenda = venda.DataVenda;
            result.Produtos = venda.Produtos;
            if(result.Status == EnumStatusVenda.AguardandoPagamento && (venda.Status == EnumStatusVenda.PagamentoAprovado || venda.Status == EnumStatusVenda.Cancelada) ||
                (result.Status == EnumStatusVenda.PagamentoAprovado && (venda.Status == EnumStatusVenda.EnviadoParaTransportadora || venda.Status == EnumStatusVenda.Cancelada)) ||
                (result.Status == EnumStatusVenda.EnviadoParaTransportadora && venda.Status == EnumStatusVenda.Entregue))
                result.Status = venda.Status;
            _context.Vendas.Update(result);
            _context.SaveChanges();
            return Ok(result);
        }

        [HttpDelete("{id}")]
        public IActionResult Deletar(int id)
        {
            var result = _context.Vendas.Find(id);
            if (result == null) return NotFound();

            _context.Vendas.Remove(result);
            _context.SaveChanges();
            return NoContent();
        }   
    }
}