using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Entities
{
    public class Venda
    {
        public int Id { get; set; }
        public virtual Vendedor Vendedor { get; set; }
        public DateTime DataVenda { get; set; }
        public string Produtos { get; set; }
        public EnumStatusVenda Status { get; set; }
    }
}